package com.cdp4j.generator;

import java.io.File;
import java.util.Arrays;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;

public class Spotless {

    public static final boolean OS_IS_WINDOWS = System.getProperty("os.name").startsWith("Windows");

    public static void main(String[] args) throws Exception {
        runMaven("spotless:apply");
    }

    private static void runMaven(final String... goals) throws MavenInvocationException {
        final InvocationRequest request = new DefaultInvocationRequest();
        request.setGoals(Arrays.asList(goals));
        request.setMavenExecutable(findBinary("mvn"));
        request.setJavaHome(new File(System.getProperty("java.home")));

        final Invoker invoker = new DefaultInvoker();
        invoker.execute(request);
    }

    private static File findBinary(final String name) {
        String path = System.getenv("PATH");
        path = "." + (path == null ? "" : File.pathSeparator + path);
        for (final String dir : path.split(File.pathSeparator)) {
            File candidate = new File(dir, name);
            if (candidate.canExecute()) return candidate;
            if (OS_IS_WINDOWS) {
                candidate = new File(dir, name + ".exe");
                if (candidate.canExecute()) return candidate;
                candidate = new File(dir, name + ".cmd");
                if (candidate.canExecute()) return candidate;
                candidate = new File(dir, name + ".bat");
                if (candidate.canExecute()) return candidate;
            }
        }
        return null;
    }
}
