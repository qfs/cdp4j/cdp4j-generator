package com.cdp4j.generator;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.write;
import static java.util.Arrays.asList;
import static java.util.Collections.unmodifiableSet;

import com.cdp4j.TypeReference;
import com.cdp4j.exception.CdpException;
import com.cdp4j.session.CommandReturnType;
import com.cdp4j.session.ParameterizedCommand;
import com.cdp4j.session.ParameterizedCommandImpl;
import com.cdp4j.session.SessionInvocationHandler;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import javax.lang.model.element.Modifier;
import org.jboss.forge.roaster.Roaster;
import org.jboss.forge.roaster.model.Type;
import org.jboss.forge.roaster.model.source.FieldSource;
import org.jboss.forge.roaster.model.source.Import;
import org.jboss.forge.roaster.model.source.JavaClassSource;
import org.jboss.forge.roaster.model.source.JavaInterfaceSource;
import org.jboss.forge.roaster.model.source.MethodSource;
import org.jboss.forge.roaster.model.source.ParameterSource;

public class CommandGenerator {

    private static final Set<String> IGNORED_EMPTY = unmodifiableSet(new HashSet<>(
            asList("Tethering", "BackgroundService", "CustomCommand", "DOMDebugger", "IO", "PerformanceTimeline")));

    private static class CommandInfo {
        public String domain;
        public String command;
        public String returns;

        public CommandInfo(String domain, String command) {
            this.domain = domain;
            this.command = command;
        }

        @Override
        public int hashCode() {
            return Objects.hash(command, domain);
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            CommandInfo other = (CommandInfo) obj;
            return Objects.equals(command, other.command) && Objects.equals(domain, other.domain);
        }
    }

    public static void main(String[] args) throws Exception {
        List<String> commandLines = Files.readAllLines(Paths.get("commands.txt"));

        List<CommandInfo> commandReturnNames = new ArrayList<>();

        List<String> domains = new ArrayList<>();
        List<String> commands = new ArrayList<>();

        for (String nextLine : commandLines) {
            String[] split = nextLine.split(",");
            CommandInfo info = new CommandInfo(split[0].trim(), split[1].trim());
            info.returns = split[2].trim();
            commandReturnNames.add(info);
        }

        List<Path> list =
                Files.list(Paths.get("src/main/java/com/cdp4j/command")).collect(Collectors.toList());

        for (Path interfacePath : list) {
            String name = interfacePath.toString();
            if (name.endsWith("Factory.java")
                    || name.endsWith("Async.java")
                    || name.endsWith("Impl.java")
                    || Files.isDirectory(interfacePath)) {
                continue;
            }

            JavaInterfaceSource cmdInterface = Roaster.parse(JavaInterfaceSource.class, interfacePath.toFile());

            generateAsync(cmdInterface);

            String domain = cmdInterface.getName();
            if (!domains.contains(domain)) {
                domains.add(domain);
            }
            JavaClassSource cmdImpl = Roaster.create(JavaClassSource.class);

            for (Import anImport : cmdInterface.getImports()) {
                cmdImpl.addImport(anImport);
            }

            cmdImpl.addImport("com.cdp4j.session.DomainCommand");
            cmdImpl.addImport(ParameterizedCommandImpl.class.getName());

            MethodSource<JavaClassSource> cmdImplConstructor = cmdImpl.addMethod();
            cmdImplConstructor.setConstructor(true);
            cmdImplConstructor.setPublic();
            cmdImplConstructor.addParameter(SessionInvocationHandler.class, "handler");
            cmdImplConstructor.setBody("this.handler = handler;");

            if (!IGNORED_EMPTY.contains(domain)) {
                // private final Object[] EMPTY = new Object[] { };
                FieldSource<JavaClassSource> emptyField = cmdImpl.addField();
                emptyField.setPrivate();
                emptyField.setStatic(true);
                emptyField.setFinal(true);
                emptyField.setType(Object[].class);
                emptyField.setName("EMPTY_VALUES");
                emptyField.setLiteralInitializer("new Object[] { }");

                // private final Object[] EMPTY = new Object[] { };
                FieldSource<JavaClassSource> emptyStrArgs = cmdImpl.addField();
                emptyStrArgs.setPrivate();
                emptyStrArgs.setStatic(true);
                emptyStrArgs.setFinal(true);
                emptyStrArgs.setType(String[].class);
                emptyStrArgs.setName("EMPTY_ARGS");
                emptyStrArgs.setLiteralInitializer("new String[] { }");
            }

            cmdImpl.setPackage(cmdInterface.getPackage());
            cmdImpl.setName(cmdInterface.getName() + "Impl");
            cmdImpl.setSuperType(ParameterizedCommandImpl.class.getSimpleName() + "<" + cmdInterface.getName() + ">");
            cmdImpl.implementInterface(cmdInterface);
            cmdImpl.setPackagePrivate();

            FieldSource<JavaClassSource> handlerField = cmdImpl.addField();
            handlerField.setName("handler");
            handlerField.setPrivate();
            handlerField.setFinal(true);
            handlerField.getJavaDoc().setFullText("instance fields");
            handlerField.setType(SessionInvocationHandler.class);

            for (MethodSource<JavaClassSource> next : cmdImpl.getMethods()) {
                next.removeJavaDoc();
            }

            Set<String> variableNames = new HashSet<>();
            Map<String, Integer> paramNameCounter = new HashMap<>();
            Map<String, Boolean> processedReturn = new HashMap<>();

            for (MethodSource<JavaClassSource> next : cmdImpl.getMethods()) {
                if (next.isConstructor()) {
                    continue;
                }
                String command = next.getName();
                String method = domain + "." + command;

                if (!commands.contains(method)) {
                    commands.add(method);
                }

                Type<JavaClassSource> returnType = next.getReturnType();
                Type<JavaClassSource> typeArgument =
                        !returnType.getTypeArguments().isEmpty()
                                ? returnType.getTypeArguments().get(0)
                                : null;
                boolean isVoid = returnType.getSimpleName().equalsIgnoreCase("void");

                String variableName = returnType.getName().toUpperCase(Locale.ENGLISH);
                if (typeArgument != null) {
                    variableName = returnType.getName().toUpperCase(Locale.ENGLISH) + "_"
                            + typeArgument.getName().replace(".", "_").toUpperCase(Locale.ENGLISH);
                }

                if ("getContentQuads".equals(command) && "DOM".equals(domain)) {
                    variableName = "LIST_LIST_DOUBLE";
                } else if ("getDOMStorageItems".equals(command) && "DOMStorage".equals(domain)) {
                    variableName = "LIST_LIST_STRING";
                } else if ("profileSnapshot".equals(command) && "LayerTree".equals(domain)) {
                    variableName = "LIST_LIST_DOUBLE";
                }

                if (typeArgument != null && !variableNames.contains(variableName)) {
                    cmdImpl.addImport(TypeReference.class);
                    FieldSource<JavaClassSource> field = cmdImpl.addField();
                    String tt = "TypeReference<" + returnType.getName() + "<" + typeArgument.getName() + ">" + ">";

                    if ("getContentQuads".equals(command) && "DOM".equals(domain)) {
                        tt = "TypeReference<List<List<Double>>>";
                    } else if ("getDOMStorageItems".equals(command) && "DOMStorage".equals(domain)) {
                        tt = "TypeReference<List<List<String>>>";
                    } else if ("profileSnapshot".equals(command) && "LayerTree".equals(domain)) {
                        tt = "TypeReference<List<List<Double>>>";
                    }

                    String importPackage = typeArgument
                            .getQualifiedName()
                            .replace("command", "type." + domain.toLowerCase(Locale.ENGLISH));

                    if ("Page".equals(domain)) {
                        if ("Cookie".equals(typeArgument.getName())) {
                            importPackage = importPackage.replace("type.page", "type.network");
                        } else if ("SearchMatch".equals(typeArgument.getName())) {
                            importPackage = importPackage.replace("type.page", "type.debugger");
                        }
                    }

                    if ("Network".equals(domain)) {
                        if ("SearchMatch".equals(typeArgument.getName())) {
                            importPackage = importPackage.replace("type.network", "type.debugger");
                        }
                    }

                    if ("Storage".equals(domain)) {
                        if ("Cookie".equals(typeArgument.getName())) {
                            importPackage = importPackage.replace("type.storage", "type.network");
                        }
                    }

                    cmdImpl.addImport(importPackage);

                    field.setType(tt);
                    field.setPrivate();
                    field.setStatic(true);
                    field.setFinal(true);
                    field.setLiteralInitializer("new " + tt + "(){};");
                    field.setName(variableName);

                    variableNames.add(variableName);
                }

                if (typeArgument != null) {
                    next.addAnnotation(SuppressWarnings.class).setStringValue("unchecked");
                }

                StringJoiner paramNames = new StringJoiner("\", \"");
                StringJoiner paramValues = new StringJoiner(", ");

                Integer paramNameCount = paramNameCounter.get(command);
                if (paramNameCount == null) {
                    paramNameCount = Integer.valueOf(Integer.valueOf(0).intValue() + 1);
                    paramNameCounter.put(command, paramNameCount);
                } else {
                    paramNameCount = Integer.valueOf(paramNameCount.intValue() + 1);
                    paramNameCounter.put(command, paramNameCount);
                }

                for (ParameterSource<JavaClassSource> parameterSource : next.getParameters()) {
                    paramNames.add(parameterSource.getName());
                    paramValues.add(parameterSource.getName());
                    Type<JavaClassSource> type = parameterSource.getType();
                    if (!type.isParameterized()) {
                        cmdImpl.addImport(type.getQualifiedName());
                    } else {
                        for (Type<?> nextType : type.getTypeArguments()) {
                            String importPackage = nextType.getQualifiedName()
                                    .replace("command", "type." + domain.toLowerCase(Locale.ENGLISH));
                            if ("Storage".equals(domain)
                                    && importPackage.equals("com.cdp4j.type.network.CookieParam")) {
                                importPackage = "com.cdp4j.type.network.CookieParam";
                            }
                            cmdImpl.addImport(importPackage);
                        }
                    }
                }

                String parameters = paramNames.toString().trim().isBlank()
                        ? null
                        : "new String[] {\"" + paramNames.toString() + "\"}";
                String values = paramValues.toString().trim().isBlank()
                        ? null
                        : "new Object[] { " + paramValues.toString() + " }";

                StringBuilder builder = new StringBuilder();
                if (!isVoid) {
                    if (("getContentQuads".equals(command) && "DOM".equals(domain))
                            || ("profileSnapshot".equals(command) && "LayerTree".equals(domain))) {
                        builder.append("return (List<List<Double>>) ");
                    } else if ("getDOMStorageItems".equals(command) && "DOMStorage".equals(domain)) {
                        builder.append("return (List<List<String>>) ");
                    } else if ("captureScreenshot".equals(command) && "Page".equals(domain)) {
                        builder.append("return (byte[]) ");
                    } else if ("getApplicationCacheForFrame".equals(command) && "ApplicationCache".equals(domain)) {
                        builder.append("return (com.cdp4j.type.applicationcache.ApplicationCache) ");
                    } else {
                        builder.append("return (" + returnType.getSimpleName()
                                + (typeArgument != null ? "<" + typeArgument.getName() + ">" : "") + ") ");
                    }
                }
                String paramFieldName = null;
                if (parameters != null) {
                    paramFieldName = "PARAMS_"
                            + command.replaceAll("(.)(\\p{Upper})", "$1_$2").toUpperCase(Locale.ENGLISH) + "_"
                            + paramNameCount.intValue();
                    FieldSource<JavaClassSource> paramNameField = cmdImpl.addField()
                            .setPrivate()
                            .setStatic(true)
                            .setFinal(true)
                            .setName(paramFieldName)
                            .setType(String[].class);
                    paramNameField.setLiteralInitializer(parameters.toString());
                }

                if (!processedReturn.containsKey(command)) {
                    int idx = commandReturnNames.indexOf(new CommandInfo(domain, command));
                    String returns = null;
                    if (idx >= 0) {
                        returns = commandReturnNames.get(idx).returns;
                    }
                    String returnTypeValue =
                            returnType.getSimpleName().equals("byte") ? "byte[]" : returnType.getSimpleName();
                    cmdImpl.addField()
                            .setPrivate()
                            .setStatic(true)
                            .setFinal(true)
                            .setName("CRT_"
                                    + command.replaceAll("(.)(\\p{Upper})", "$1_$2")
                                            .toUpperCase(Locale.ENGLISH))
                            .setType(CommandReturnType.class)
                            .setLiteralInitializer(format(
                                    "new CommandReturnType(%s, %s, %s)",
                                    returns != null ? "\"" + returns + "\"" : "null",
                                    returnTypeValue + ".class",
                                    typeArgument != null ? variableName : "null"));
                    processedReturn.put(command, Boolean.TRUE);
                }

                String invoke = "invoke";
                String domainCommand = domain + "_" + command;
                String postFixArg = "";
                for (CustomParser customParser : Configuration.CUSTOM_PARSERS) {
                    if (customParser.expectedDomainCommand.equals(domainCommand)) {
                        invoke = "invokeWithCustomParser";
                        domainCommand = customParser.effectiveDomainCommand;
                        postFixArg = ", " + customParser.snapshotParserClass + ".INSTANCE";
                        cmdImpl.addImport("com.cdp4j.serialization." + customParser.snapshotParserClass);
                        break;
                    }
                }

                builder.append("handler." + invoke + "(");
                builder.append("this, ");
                builder.append("DomainCommand." + domainCommand + ", ");
                builder.append(
                        "CRT_" + command.replaceAll("(.)(\\p{Upper})", "$1_$2").toUpperCase(Locale.ENGLISH));
                builder.append(parameters != null ? ", " + paramFieldName : ", EMPTY_ARGS");
                builder.append(values != null ? ", " + values.toString() : ", EMPTY_VALUES");
                builder.append(", true");
                builder.append(postFixArg);
                builder.append(");");

                next.setBody(builder.toString());
            }

            cmdImpl.removeImport(cmdImpl.getQualifiedName().replace("Impl", ""));

            if ("Console".equals(domain)) {
                cmdImpl.addAnnotation(SuppressWarnings.class).setStringValue("deprecation");
            }

            Path implFile = Paths.get(interfacePath
                            .toString()
                            .substring(0, interfacePath.toString().lastIndexOf(".")) + "Impl.java");

            List<FieldSource<JavaClassSource>> fieldsClone = new ArrayList<>();

            for (FieldSource<JavaClassSource> field : cmdImpl.getFields()) {
                fieldsClone.add(field);
                cmdImpl.removeField(field);
            }

            Collections.sort(fieldsClone, (o1, o2) -> o1.getName().compareTo(o2.getName()));

            for (FieldSource<JavaClassSource> field : fieldsClone) {
                if (field.getType().toString().startsWith("TypeReference")) {
                    cmdImpl.addField(field.toString());
                }
            }

            for (FieldSource<JavaClassSource> field : fieldsClone) {
                if (field.getType().toString().startsWith("TypeReference")) {
                    continue;
                }
                cmdImpl.addField(field.toString());
            }

            List<MethodSource<JavaClassSource>> clonedMethods = new ArrayList<>();
            for (MethodSource<JavaClassSource> next : cmdImpl.getMethods()) {
                if (next.isConstructor()) {
                    continue;
                }
                clonedMethods.add(next);
                cmdImpl.removeMethod(next);
            }

            Collections.sort(clonedMethods, (o1, o2) -> {
                if (o1.getName().equals(o2.getName())) {
                    return Integer.compare(
                            o1.getParameters().size(), o2.getParameters().size());
                } else {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            for (MethodSource<JavaClassSource> next : clonedMethods) {
                cmdImpl.addMethod(next);
            }

            Files.write(
                    implFile,
                    cmdImpl.toString().replaceAll("\t", Configuration.INDENT).getBytes(StandardCharsets.UTF_8));

            List<String> lines;
            try {
                lines = Files.readAllLines(implFile, StandardCharsets.UTF_8);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            lines.add(0, "// SPDX-License-Identifier: MIT");

            Files.write(implFile, lines, StandardCharsets.UTF_8);
            generateAsyncImpl(implFile);
        }

        Collections.sort(domains);

        generateDomainEnum(domains);

        Collections.sort(commands);
        generateCommandEnum(domains, commands);

        generateFactory(domains, false, "");
        generateFactory(domains, false, "Async");
        generateFactory(domains, true, "");
        generateFactory(domains, true, "Async");
    }

    private static void generateAsyncImpl(Path implFile) throws IOException {
        JavaClassSource asyncImpl = (JavaClassSource) Roaster.parse(implFile.toFile());
        String className = asyncImpl.getName().replace("Impl", "") + "AsyncImpl";
        asyncImpl.setName(className);
        asyncImpl.setPackage(asyncImpl.getPackage());
        asyncImpl.addImport(CompletableFuture.class);
        asyncImpl.removeInterface(asyncImpl.getInterfaces().get(0));
        asyncImpl.setSuperType(asyncImpl.getSuperType().replace(">", "Async>"));
        asyncImpl.addInterface(asyncImpl.getName().replace("Impl", ""));
        for (MethodSource<JavaClassSource> next : asyncImpl.getMethods()) {
            if (next.isConstructor()) {
                continue;
            }
            if (next.getAnnotation(SuppressWarnings.class) == null) {
                next.addAnnotation(SuppressWarnings.class).setStringValue("unchecked");
            }
            String returnType = next.getReturnType().toString();
            if ("void".equals(returnType)) {
                returnType = "Void";
            }
            String returnTypePromise = "CompletableFuture<" + returnType + ">";
            next.setReturnType(returnTypePromise);
            String body = next.getBody();
            if (body.startsWith("return")) {
                String newBody = body.replace("return (" + returnType + ")", "return (" + returnTypePromise + ")");
                next.setBody(newBody.replace(",true", ",false"));
            } else {
                next.setBody(
                        "return (CompletableFuture<Void>) " + next.getBody().replace(",true", ",false"));
            }
        }
        Path asyncPath = Paths.get("src/main/java/com/cdp4j/command").toAbsolutePath();
        if (!Files.exists(asyncPath)) {
            try {
                Files.createDirectories(asyncPath);
            } catch (IOException e) {
                throw new CdpException(e);
            }
        }
        asyncPath = asyncPath.resolve(className + ".java");
        try {
            if (Files.exists(asyncPath)) {
                Files.delete(asyncPath);
            }
            Files.write(
                    asyncPath,
                    asyncImpl.toString().replaceAll("\t", Configuration.INDENT).getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE_NEW);
        } catch (IOException e) {
            throw new CdpException(e);
        }
    }

    private static void generateAsync(JavaInterfaceSource cmdInterface) {
        JavaInterfaceSource asyncInterface = (JavaInterfaceSource) Roaster.parse(cmdInterface.toString());
        final String syncName = asyncInterface.getName();
        String className = syncName + "Async";
        asyncInterface.setName(className);
        asyncInterface.removeInterface(ParameterizedCommand.class.getSimpleName() + "<" + syncName + ">");
        asyncInterface.addInterface(ParameterizedCommand.class.getSimpleName() + "<" + className + ">");
        asyncInterface.setPackage(cmdInterface.getPackage());
        asyncInterface.addImport(CompletableFuture.class);
        for (MethodSource<JavaInterfaceSource> next : asyncInterface.getMethods()) {
            String returnType = next.getReturnType().toString();
            if ("void".equals(returnType)) {
                returnType = "Void";
            }
            next.setReturnType("CompletableFuture<" + returnType + ">");
        }
        Path asyncPath = Paths.get("src/main/java/com/cdp4j/command").toAbsolutePath();
        if (!Files.exists(asyncPath)) {
            try {
                Files.createDirectories(asyncPath);
            } catch (IOException e) {
                throw new CdpException(e);
            }
        }
        asyncPath = asyncPath.resolve(className + ".java");
        try {
            if (Files.exists(asyncPath)) {
                Files.delete(asyncPath);
            }
            Files.write(
                    asyncPath,
                    asyncInterface
                            .toString()
                            .replace("\t", Configuration.INDENT)
                            .getBytes(StandardCharsets.UTF_8),
                    StandardOpenOption.CREATE_NEW);
        } catch (IOException e) {
            throw new CdpException(e);
        }
    }

    private static void generateDomainEnum(List<String> domains) throws IOException {
        StringBuilder domainsBuilder = new StringBuilder();
        domainsBuilder.append("// SPDX-License-Identifier: MIT\r\n");
        domainsBuilder.append("package com.cdp4j.session;").append("\r\n");
        domainsBuilder.append("\r\n");
        domainsBuilder.append("public enum Domain {").append("\r\n");

        int i = 0;
        for (String next : domains) {
            domainsBuilder
                    .append("  ")
                    .append(next)
                    .append(i != domains.size() - 1 ? "," : "")
                    .append("\r\n");
            i += 1;
        }
        domainsBuilder.append("}\r\n");

        StringBuilder domainsEnumStr = new StringBuilder();
        domainsEnumStr.append("// SPDX-License-Identifier: MIT\r\n");
        Path domainsEnumPath = Paths.get("src/main/java")
                .resolve("com")
                .resolve("cdp4j")
                .resolve("session")
                .resolve("Domain.java");
        write(domainsEnumPath, domainsBuilder.toString().getBytes(UTF_8));
    }

    private static void generateCommandEnum(List<String> domains, List<String> commands) throws IOException {
        StringBuilder commandsBuilder = new StringBuilder();
        commandsBuilder.append("// SPDX-License-Identifier: MIT\r\n");
        commandsBuilder.append("package com.cdp4j.session;").append("\r\n");
        commandsBuilder.append("\r\n");

        for (String domain : domains) {
            commandsBuilder.append("import static com.cdp4j.session.Domain." + domain + ";\r\n");
        }

        commandsBuilder.append("\r\npublic enum DomainCommand {").append("\r\n");

        int i = 0;
        for (String next : commands) {
            String domain = next.split("\\.")[0];
            String command = next.split("\\.")[1];
            commandsBuilder
                    .append("  ")
                    .append(domain + "_" + command + "(" + domain + ", \"" + command + "\")")
                    .append(i != commands.size() - 1 ? "," : ";")
                    .append("\r\n");
            i += 1;
        }

        commandsBuilder.append("    \r\n"
                + "  public final Domain   domain;\r\n\r\n"
                + "  public final String   command;\r\n\r\n"
                + "  public final String   method;\r\n\r\n"
                + "  DomainCommand(Domain domain, String command) {\r\n"
                + "      this.domain     = domain;\r\n"
                + "      this.command    = command;\r\n"
                + "      this.method     = domain.name() + \".\" + command;\r\n"
                + "  }\r\n"
                + "\r\n"
                + "  @Override\r\n"
                + "  public String toString() {\r\n"
                + "      return this.method;\r\n"
                + "  }\r\n"
                + "");

        commandsBuilder.append("}\r\n");

        Path commandsBuilderPath = Paths.get("src/main/java")
                .resolve("com")
                .resolve("cdp4j")
                .resolve("session")
                .resolve("DomainCommand.java");

        write(commandsBuilderPath, commandsBuilder.toString().getBytes(UTF_8));
    }

    private static void generateFactory(
            final List<String> domains, final boolean generateWrapper, final String asyncString) {
        String wrapperClassName = asyncString + "Command";
        String factoryClassName = asyncString + "CommandFactory";

        TypeSpec.Builder factoryBuilder = TypeSpec.classBuilder(generateWrapper ? wrapperClassName : factoryClassName)
                .addModifiers(Modifier.PUBLIC);

        for (final String domain : domains) {
            final ClassName className = ClassName.get("com.cdp4j.command", domain + asyncString);
            final String fieldName = toVarName(domain);
            factoryBuilder.addField(className, fieldName, Modifier.PRIVATE);
        }

        if (generateWrapper) {
            factoryBuilder.addField(
                    ClassName.get("com.cdp4j.command", factoryClassName), "delegate", Modifier.PRIVATE, Modifier.FINAL);

            factoryBuilder.addMethod(MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(SessionInvocationHandler.class, "handler", Modifier.FINAL)
                    .addStatement("this.$N = new $N($N)", "delegate", factoryClassName, "handler")
                    .build());
        } else {
            factoryBuilder.addField(SessionInvocationHandler.class, "handler", Modifier.PRIVATE, Modifier.FINAL);

            factoryBuilder.addMethod(MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(SessionInvocationHandler.class, "handler", Modifier.FINAL)
                    .addStatement("this.$N = $N", "handler", "handler")
                    .build());
        }

        for (final String domain : domains) {
            final ClassName className = ClassName.get("com.cdp4j.command", domain + asyncString);
            final String fieldName = toVarName(domain);
            final String getterName = "get" + domain;
            final String initializer =
                    generateWrapper ? "delegate." + getterName + "()" : "new " + domain + asyncString + "Impl(handler)";
            factoryBuilder.addMethod(MethodSpec.methodBuilder(getterName)
                    .returns(className)
                    .addModifiers(Modifier.PUBLIC)
                    .addStatement(
                            "if ($N == null) {\n" + "    return $N = $N;\n" + "}\n" + "return $N",
                            fieldName,
                            fieldName,
                            initializer,
                            fieldName)
                    .build());
        }

        JavaFile javaFile = JavaFile.builder(
                        generateWrapper ? "com.cdp4j.session" : "com.cdp4j.command", factoryBuilder.build())
                .addFileComment(Generator.LICENSE_TEXT_TEMPLATE)
                .skipJavaLangImports(true)
                .indent(Configuration.INDENT)
                .build();
        Path rootPath = Paths.get("src/main/java");
        try {
            javaFile.writeTo(rootPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Generator.addLicenseHeader(javaFile, rootPath);
    }

    private static String toVarName(final String name) {
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            final char character = name.charAt(i);
            if (Character.isUpperCase(character)) {
                if (i == 0
                        || i == name.length() - 1
                        || (i < name.length() - 1 && Character.isUpperCase(name.charAt(i + 1)))) {
                    sb.append(Character.toLowerCase(character));
                    continue;
                }
            }
            sb.append(name.substring(i));
            break;
        }

        return sb.toString();
    }
}
