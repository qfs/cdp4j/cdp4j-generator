package com.cdp4j.generator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws Exception {
        if (Files.exists(Paths.get("commands.txt"))) {
            Files.delete(Paths.get("commands.txt"));
        }
        System.out.println("Cleaning output directories...");
        cleanOutputDirs();
        System.out.println("Generating Interfaces and Types...");
        Generator.main(args);
        System.out.println("Generating Commands...");
        CommandGenerator.main(args);
        System.out.println("Adding ToString...");
        ToStringGenerator.main(args);
        System.out.println("Running Spotless...");
        Spotless.main(args);
        System.out.println("Removing temp file...");
        try {
            Files.delete(Paths.get("commands.txt"));
        } catch (NoSuchFileException e) {
            // ignore
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void cleanOutputDirs() throws IOException {
        deletePath("src/main/java/com/cdp4j/command");
        deletePath("src/main/java/com/cdp4j/event");
        deletePath("src/main/java/com/cdp4j/type", "util");
    }

    private static void deletePath(final String pathToBeDeleted, String... skips) throws IOException {
        Set<Path> skipPaths = Arrays.stream(skips)
                .map(skip -> Paths.get(pathToBeDeleted + File.separator + skip))
                .collect(Collectors.toSet());
        try {
            Files.walk(Paths.get(pathToBeDeleted))
                    .sorted(Comparator.reverseOrder())
                    .filter(p -> {
                        while (p != null) {
                            if (skipPaths.contains(p)) return false;
                            p = p.getParent();
                        }
                        return true;
                    })
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (final NoSuchFileException ex) {
            /* ignore */
        }
    }
}
