package com.cdp4j.generator;

public class CustomParser {
    final String expectedDomainCommand;
    final String effectiveDomainCommand;
    final String snapshotParserClass;

    public CustomParser(
            final String expectedDomainCommand, final String effectiveDomainCommand, final String snapshotParserClass) {
        this.expectedDomainCommand = expectedDomainCommand;
        this.effectiveDomainCommand = effectiveDomainCommand;
        this.snapshotParserClass = snapshotParserClass;
    }
}
