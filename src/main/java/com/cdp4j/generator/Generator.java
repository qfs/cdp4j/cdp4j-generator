package com.cdp4j.generator;

import static java.lang.Character.isLetterOrDigit;
import static java.lang.Character.isUpperCase;
import static java.lang.Character.toLowerCase;
import static java.lang.Character.toUpperCase;
import static javax.lang.model.element.Modifier.FINAL;
import static javax.lang.model.element.Modifier.PUBLIC;

import com.cdp4j.annotation.Experimental;
import com.cdp4j.annotation.Optional;
import com.cdp4j.session.ParameterizedCommand;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.rits.cloning.Cloner;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ArrayTypeName;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeSpec.Builder;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.lang.model.element.Modifier;

public class Generator {

    public static final String COMMAND_PACKAGE = "com.cdp4j.command";
    private static List<Map<String, Object>> domains;

    protected static final String LICENSE_TEXT_TEMPLATE = "SPDX-License-Identifier: MIT";

    private static final Set<String> COMMANDS = new HashSet<>();

    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws IOException {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Map<String, Object> browserProtocol = gson.fromJson(
                new FileReader(Paths.get("src/test/resources/protocol/browser_protocol.json")
                        .toFile()),
                Map.class);
        Map<String, Object> jsProtocolProtocol = gson.fromJson(
                new FileReader(Paths.get("src/test/resources/protocol/js_protocol.json")
                        .toFile()),
                Map.class);
        Map<String, Object> customProtocolProtocol = gson.fromJson(
                new FileReader(Paths.get("src/test/resources/protocol/custom_protocol.json")
                        .toFile()),
                Map.class);

        List<Map<String, Object>> browserDomains = (List<Map<String, Object>>) browserProtocol.get("domains");
        List<Map<String, Object>> jsDomains = (List<Map<String, Object>>) jsProtocolProtocol.get("domains");
        List<Map<String, Object>> customDomains = (List<Map<String, Object>>) customProtocolProtocol.get("domains");

        domains = new ArrayList<>();

        domains.addAll(browserDomains);
        domains.addAll(jsDomains);
        domains.addAll(customDomains);

        Map<String, String> eventsMap = new LinkedHashMap<String, String>();

        Map<String, String> eventDescriptions = new LinkedHashMap<>();

        for (Map<String, Object> next : domains) {
            String domain = (String) next.get("domain");

            if (isList(next, "types")) {
                generateTypes(domain, (List<Map<String, Object>>) next.get("types"));
            }

            List<Map<String, Object>> events = (List<Map<String, Object>>) next.get("events");
            if (isList(next, "events")) {
                generateEvents(domain, events);
            }

            if (isList(next, "commands")) {
                Object object = next.get("experimental");
                boolean expr = object != null && Boolean.TRUE.equals(object);
                generateMethods(domain, (List<Map<String, Object>>) next.get("commands"), expr);
            }

            if (events != null) {
                for (Map<String, Object> event : events) {
                    Object edesc = event.get("description");
                    if (edesc != null && !edesc.toString().trim().isEmpty()) {
                        eventDescriptions.put(domain + "." + event.get("name"), (String) edesc);
                    }
                    eventsMap.put(domain + "." + event.get("name"), toCamelCase((String) event.get("name")));
                }
            }
        }

        Builder events = TypeSpec.classBuilder("Events").addModifiers(Modifier.PUBLIC);
        events.addField(FieldSpec.builder(
                        ParameterizedTypeName.get(Map.class, String.class, Class.class), "events", Modifier.PUBLIC)
                .initializer("new $T<>()", HashMap.class)
                .build());

        com.squareup.javapoet.CodeBlock.Builder codeblock = CodeBlock.builder();

        for (Map.Entry<String, String> n : eventsMap.entrySet()) {
            String domain = n.getKey();

            codeblock.add("events.put(\"" + domain + "\", " + toCamelCase((String) n.getValue()) + ".class" + ");\r\n");
        }

        events.addInitializerBlock(codeblock.build());

        generateEventEnum(eventsMap, eventDescriptions);
    }

    protected static void generateEventEnum(Map<String, String> events, Map<String, String> descriptions) {

        Builder builder = TypeSpec.enumBuilder("Events")
                .addModifiers(Modifier.PUBLIC)
                .addAnnotation(AnnotationSpec.builder(SuppressWarnings.class)
                        .addMember("value", "$S", "rawtypes")
                        .build())
                .addField(String.class, "domain", PUBLIC, FINAL)
                .addField(String.class, "name", PUBLIC, FINAL)
                .addField(Class.class, "klass", PUBLIC, FINAL)
                .addMethod(MethodSpec.methodBuilder("toString")
                        .addAnnotation(Override.class)
                        .returns(String.class)
                        .addModifiers(PUBLIC)
                        .addStatement("return domain + \".\" + name")
                        .build())
                .addMethod(MethodSpec.constructorBuilder()
                        .addParameter(String.class, "domain")
                        .addParameter(String.class, "name")
                        .addParameter(Class.class, "klass")
                        .addStatement("this.$N = $N", "domain", "domain")
                        .addStatement("this.$N = $N", "name", "name")
                        .addStatement("this.$N = $N", "klass", "klass")
                        .build());

        for (Map.Entry<String, String> next : events.entrySet()) {
            String name = next.getKey();
            String domain = name.substring(0, name.indexOf("."));
            String eventName = name.substring(name.indexOf(".") + 1, name.length());
            if (name.contains(".")) {
                name = name.replace('.', '_');
            }
            if (Character.isDigit(name.charAt(0))) {
                name = "_" + name;
            }
            ClassName eventQualifiedName =
                    ClassName.get("com.cdp4j.event." + domain.toLowerCase(Locale.ENGLISH), toCamelCase(eventName));
            Builder eventValue = TypeSpec.anonymousClassBuilder(CodeBlock.builder()
                    .add("$S, $S, $T.class", domain, eventName, eventQualifiedName)
                    .build());
            String classDescription = descriptions.get(domain + "." + eventName);
            if (classDescription != null && !classDescription.trim().isEmpty()) {
                classDescription = classDescription.replace("$x", "x");
                eventValue.addJavadoc("$L", classDescription.trim().replace("`", ""));
            }
            builder.addEnumConstant(toCamelCase(domain) + toCamelCase(eventName), eventValue.build());
        }
        TypeSpec typeSpec = builder.build();
        JavaFile javaFile = JavaFile.builder("com.cdp4j.event", typeSpec)
                .addFileComment(LICENSE_TEXT_TEMPLATE)
                .indent(Configuration.INDENT)
                .build();
        try {
            javaFile.writeToFile(new File("src/main/java"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    protected static void generateTypes(String domain, List<Map<String, Object>> types) {
        if (types.isEmpty()) {
            return;
        }
        for (Map<String, Object> next : types) {

            String id = (String) next.get("id");
            if (id == null || id.trim().isEmpty()) {
                continue;
            }

            String type = (String) next.get("type");
            if (type == null || type.trim().isEmpty()) {
                continue;
            }

            boolean isObject = "object".equals(type);
            boolean isEnum = next.containsKey("enum") && isList(next, "enum") && "string".equals(type);

            if (isObject) {
                if (isList(next, "properties")) {
                    List<Map<String, Object>> properties = (List<Map<String, Object>>) next.get("properties");
                    Object experimental = next.get("experimental");
                    boolean exp = experimental != null && Boolean.TRUE.equals(experimental) ? true : false;
                    generateBean(domain, properties, id, (String) next.get("description"), exp, id);
                }
            } else if (isEnum) {
                generateEnum(domain, next, id);
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected static void generateEvents(String domain, List<Map<String, Object>> events) {
        if (events.isEmpty()) {
            return;
        }
        for (Map<String, Object> next : events) {

            String name = (String) next.get("name");
            if (name == null || name.trim().isEmpty()) {
                continue;
            }

            List<Map<String, Object>> parameters = (List<Map<String, Object>>) next.get("parameters");
            Object experimental = next.get("experimental");
            boolean exp = experimental != null && Boolean.TRUE.equals(experimental) ? true : false;
            generateEvent(domain, parameters, name, (String) next.get("description"), exp);
        }
    }

    @SuppressWarnings("unchecked")
    protected static void generateEnum(
            final String domain, final Map<String, Object> properties, final String className) {

        if (!isList(properties, "enum")) {
            return;
        }

        Builder builder = TypeSpec.enumBuilder(className)
                .addModifiers(PUBLIC)
                .addField(String.class, "value", PUBLIC, FINAL)
                .addMethod(MethodSpec.methodBuilder("toString")
                        .addAnnotation(Override.class)
                        .returns(String.class)
                        .addModifiers(Modifier.PUBLIC)
                        .addStatement("return value")
                        .build())
                .addMethod(MethodSpec.constructorBuilder()
                        .addParameter(String.class, "value")
                        .addStatement("this.$N = $N", "value", "value")
                        .build());

        String classDescription = (String) properties.get("description");
        if (classDescription != null) {
            classDescription = classDescription.replace("$x", "x");
            builder.addJavadoc("$L", classDescription.replace("`", "").trim());
        }

        List<String> constants = (List<String>) properties.get("enum");
        for (String next : constants) {
            String name = toCamelCase(next);
            if (name.contains("-")) {
                name = name.replace('-', '_');
            }

            if (Character.isDigit(name.charAt(0))) {
                name = "_" + name;
            }
            builder.addEnumConstant(
                    name,
                    TypeSpec.anonymousClassBuilder("$S", next)
                            .addAnnotation(AnnotationSpec.builder(SerializedName.class)
                                    .addMember("value", "\"" + next + "\"")
                                    .build())
                            .addAnnotation(AnnotationSpec.builder(JsonProperty.class)
                                    .addMember("value", "\"" + next + "\"")
                                    .build())
                            .build());
        }

        builder.addEnumConstant(
                "_UNKNOWN_", TypeSpec.anonymousClassBuilder("$S", "_UNKNOWN_").build());

        TypeSpec typeSpec = builder.build();
        writeType(domain, typeSpec, false, false);
    }

    @SuppressWarnings("unchecked")
    protected static void generateMethods(final String domain, final List<Map<String, Object>> commands, boolean expr) {

        Builder builder = TypeSpec.interfaceBuilder(domain)
                .addModifiers(Modifier.PUBLIC)
                .addSuperinterface(ParameterizedTypeName.get(
                        ClassName.get(ParameterizedCommand.class), ClassName.get(COMMAND_PACKAGE, domain)));

        if ("Console".equals(domain)) {
            builder.addAnnotation(Deprecated.class);
        }

        if (expr) {
            builder.addAnnotation(Experimental.class);
        }

        // AnnotationSpec domainAnnotation = AnnotationSpec.builder(Domain.class).addMember("value", "\"" + domain +
        // "\"").build();
        // builder.addAnnotation(domainAnnotation);

        for (Map<String, Object> next : domains) {
            if (domain.equals(next.get("domain"))) {
                String classDescription = (String) next.get("description");
                if (classDescription != null) {
                    classDescription = classDescription.replace("$x", "x");
                    builder.addJavadoc("$L", classDescription.replace("`", ""));
                }
            }
        }

        List<Map<String, Object>> clonedList = new ArrayList<>();

        for (Map<String, Object> nextt : commands) {

            List<Map<String, Object>> parameters = (List<Map<String, Object>>) nextt.get("parameters");
            if (parameters != null) {
                Boolean optional = Boolean.FALSE;
                for (Map<String, Object> map : parameters) {
                    optional = (Boolean) map.get("optional");
                    if (Boolean.TRUE.equals(optional)) {
                        break;
                    } else {
                        optional = Boolean.FALSE;
                    }
                }
                if (optional) {
                    Map<String, Object> clone =
                            (Map<String, Object>) Cloner.shared().deepClone(nextt);
                    List<Map<String, Object>> parameters2 = (List<Map<String, Object>>) clone.get("parameters");
                    List<Map<String, Object>> fooo2 = new ArrayList<>();
                    if (parameters2 != null) {
                        Boolean optional2 = Boolean.FALSE;
                        for (Map<String, Object> map : parameters2) {
                            optional2 = Boolean.TRUE.equals(map.get("optional"));
                            if (Boolean.FALSE.equals(optional2)) {
                                fooo2.add(map);
                            }
                        }
                    }
                    if (fooo2.isEmpty()) {
                        clone.remove("parameters");
                    } else {
                        clone.put("parameters", fooo2);
                    }
                    clonedList.add(clone);
                }
            }
        }

        commands.addAll(clonedList);

        Collections.sort(commands, new Comparator<Map<String, Object>>() {

            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                String name1 = String.valueOf(o1.get("name"));
                String name2 = String.valueOf(o2.get("name"));
                List<?> params1 = (List<?>) o1.get("parameters");
                int params1Length = params1 != null ? params1.size() : 0;
                List<?> params2 = (List<?>) o2.get("parameters");
                int params2Length = params2 != null ? params2.size() : 0;
                if (name1.equals(name2)) {
                    return Integer.compare(params1Length, params2Length);
                }
                return name1.compareTo(name2);
            }
        });

        Set<String> signatures = new HashSet<>();
        for (Map<String, Object> next : commands) {
            String name = (String) next.get("name");

            boolean experimental = Boolean.TRUE.equals(next.get("experimental"));
            com.squareup.javapoet.MethodSpec.Builder methodBuilder = MethodSpec.methodBuilder(name);
            com.squareup.javapoet.MethodSpec.Builder methodSignatureBuilder = MethodSpec.methodBuilder(name);

            if (experimental) {
                methodBuilder.addAnnotation(Experimental.class);
            }
            boolean isMethodDeprecated = Boolean.TRUE.equals(next.get("deprecated"));
            if (isMethodDeprecated) {
                methodBuilder.addAnnotation(Deprecated.class);
            }
            Set<String> skippedParamNames = new HashSet<>();
            if (next.containsKey("parameters")) {
                List<Map<String, Object>> parameters = (List<Map<String, Object>>) next.get("parameters");
                for (Map<String, Object> parameter : parameters) {

                    String paramName = (String) parameter.get("name");

                    boolean exprParam = Boolean.TRUE.equals(parameter.get("experimental"));

                    boolean isRef = parameter.containsKey("$ref");
                    String type = (String) parameter.get("type");
                    boolean optional =
                            parameter.containsKey("optional") && Boolean.TRUE.equals(parameter.get("optional"));
                    boolean deprecated =
                            parameter.containsKey("deprecated") && Boolean.TRUE.equals(parameter.get("deprecated"));

                    TypeName typeName = null;

                    boolean isMap = false;
                    boolean isArray = false;

                    if ("array".equals(type) && !isRef) {
                        Map<String, Object> items = (Map<String, Object>) parameter.get("items");
                        if (items.containsKey("$ref")) {
                            parameter = items;
                            isRef = true;
                            isArray = true;
                        }
                    }

                    if (isRef) {
                        String ref = (String) parameter.get("$ref");
                        String refDomain = domain;
                        String refType = ref;
                        int start = ref.lastIndexOf(".");
                        if (start > 0) {
                            refDomain = ref.substring(0, start);
                            refType = ref.substring(start + 1, ref.length());
                        }
                        Map<String, Object> modelRefType = getRefType(refDomain, refType);
                        if (modelRefType != null) {
                            type = (String) modelRefType.get("type");
                            if ("object".equals(type)) {

                                isMap = (refDomain + "." + refType).equals("Network.Headers") ? true : false;

                                if (isMap) {
                                    typeName = ParameterizedTypeName.get(Map.class, String.class, Object.class);
                                } else {
                                    typeName = ClassName.get(
                                            "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                                }
                            }
                            if ("array".equals(type) || isArray) {
                                Map<String, Object> items = (Map<String, Object>) modelRefType.get("items");
                                if (items != null) {
                                    String parameterizedType = (String) items.get("type");
                                    TypeName parameterizedJavaType = toJavaType(parameterizedType);
                                    if (parameterizedJavaType != null) {
                                        typeName = ParameterizedTypeName.get(
                                                ClassName.get(List.class), parameterizedJavaType.box());
                                        isArray = true;
                                    } else {
                                        String itemRef = (String) items.get("$ref");
                                        if (itemRef != null) {
                                            String itemRefDomain = refDomain;
                                            String itemRefType = itemRef;
                                            int itemStart = itemRef.lastIndexOf(".");
                                            if (itemStart > 0) {
                                                itemRefDomain = itemRef.substring(0, start);
                                                itemRefType = itemRef.substring(start + 1, itemRef.length());
                                            }
                                            final ClassName itemTypeName = ClassName.get(
                                                    "com.cdp4j.type." + itemRefDomain.toLowerCase(Locale.ENGLISH),
                                                    itemRefType);
                                            typeName =
                                                    ParameterizedTypeName.get(ClassName.get(List.class), itemTypeName);
                                        }
                                    }
                                } else {
                                    if (modelRefType.containsKey("id")) {

                                        TypeName originalRefType = toJavaType(refType);
                                        if (originalRefType == null && modelRefType.containsKey("type")) {
                                            originalRefType = toJavaType((String) modelRefType.get("type"));
                                            if (originalRefType != null
                                                    && originalRefType
                                                            .toString()
                                                            .equals("int")) {
                                                originalRefType = originalRefType.box();
                                            }
                                        }

                                        if (originalRefType == null) {
                                            originalRefType = ClassName.get(
                                                    "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                                        }

                                        typeName =
                                                ParameterizedTypeName.get(ClassName.get(List.class), originalRefType);
                                    }
                                }
                            }
                        }
                    } else {
                        typeName = toJavaType(type);
                        if (typeName != null) {
                            if (typeName.isBoxedPrimitive()) {
                                typeName = typeName.box();
                            }
                        }
                    }

                    if (typeName == null) {
                        typeName = toJavaType(type);
                        if (typeName != null) {
                            if (typeName.isBoxedPrimitive()) {
                                typeName = typeName.box();
                            }
                        }
                    }

                    if ("this".equals(name)) {
                        name = "that";
                    }

                    if ("array".equals(type) && typeName == null && parameter.containsKey("items")) {
                        Map<String, Object> items = (Map<String, Object>) parameter.get("items");
                        if (items != null) {
                            type = (String) items.get("type");
                            typeName = toJavaType(type);
                            if (typeName != null) {
                                typeName = ParameterizedTypeName.get(ClassName.get(List.class), typeName.box());
                            }
                        }
                    }

                    if (typeName == null) {
                        skippedParamNames.add(paramName);
                        continue;
                    }

                    if (parameter.containsKey("enum") && "string".equals(type)) {

                        String enumName = "";

                        if (enumName.isEmpty()) {
                            if ("setTouchEmulationEnabled".equals(name)) {
                                enumName = "Platform";
                            } else if ("captureScreenshot".equals(name) || "startScreencast".equals(name)) {
                                enumName = "ImageFormat";
                            } else if ("dispatchKeyEvent".equals(name)) {
                                enumName = "KeyEventType";
                            } else if ("dispatchMouseEvent".equals(name)) {
                                enumName = "MouseEventType";
                                /*
                                List<String> enums = (List<String>) parameter.get("enum");
                                if (enums.contains("mousePressed")) {
                                    enumName = "MouseEventType";
                                } else if (enums.contains("none")) {
                                    enumName = "MouseButtonType";
                                }*/
                            } else if ("dispatchTouchEvent".equals(name)) {
                                enumName = "TouchEventType";
                            } else if ("emulateTouchFromMouseEvent".equals(name)) {
                                if ("type".equals(parameter.get("name"))) {
                                    enumName = "MouseEventType";
                                } else if ("button".equals(parameter.get("name"))) {
                                    enumName = "MouseButtonType";
                                }
                            } else if ("start".equals(name)) {
                                enumName = "TransferMode";
                            } else if ("setPauseOnExceptions".equals(name)) {
                                enumName = "PauseOnExceptionState";
                            } else if ("getEncodedResponse".equals(name)) {
                                enumName = "Encoding";
                            } else if ("setDownloadBehavior".equals(name)) {
                                enumName = "DownloadBehavior";
                            } else if ("setEmitTouchEventsForMouse".equals(name)) {
                                enumName = "Platform";
                            } else if ("setEmulatedVisionDeficiency".equals(name)) {
                                enumName = "EmulatedVisionDeficiency";
                            } else if ("captureSnapshot".equals(name)) {
                                enumName = "SnapshotType";
                            } else if ("printToPDF".equals(name)) {
                                enumName = "TransferMode";
                            } else if ("setWebLifecycleState".equals(name)) {
                                enumName = "WebLifecycleState";
                            } else if (("enable".equals(name) || "setTimeDomain".equals(name))
                                    && "timeDomain".equals(paramName)) {
                                enumName = "TimeDomain";
                            } else if ("continueToLocation".equals(name) && "targetCallFrames".equals(paramName)) {
                                enumName = "TargetCallFrames";
                            } else if ("setInstrumentationBreakpoint".equals(name)
                                    && ("instrumentation".equals(name)
                                            || "setInstrumentationBreakpoint".equals(name))) {
                                enumName = "InstrumentationName";
                            } else if (name.equals("dispatchDragEvent")) {
                                enumName = "DragEvent"; // historical Reasons
                            }
                        }

                        if ("transferMode".equals(paramName) && "Tracing".equals(domain)) {
                            enumName = "TracingTransferMode";
                        }

                        if ("pointerType".equals(paramName) && "Input".equals(domain)) {
                            enumName = "PointerType";
                        } else if ("setSPCTransactionMode".equals(name)) {
                            enumName = "SPCTransactionMode";
                        }

                        if (name.equals("enable") && paramName.equals("includeWhitespace") && "DOM".equals(domain)) {
                            enumName = "IncludeWhitespace";
                        }

                        if (name.equals("restartFrame") && paramName.equals("mode")) {
                            enumName = "RestartFrameMode";
                        }

                        if (name.equals("getElementByRelation") && paramName.equals("relation")) {
                            enumName = "ElementRelation";
                        }

                        if (enumName == null || enumName.trim().isEmpty()) {
                            System.err.println("Empty enum name for domain: " + domain + ", name: " + name
                                    + ", paramName: " + paramName);
                        }

                        Builder enumBuilder = TypeSpec.enumBuilder(enumName)
                                .addModifiers(Modifier.PUBLIC)
                                .addField(String.class, "value", Modifier.PUBLIC, Modifier.FINAL)
                                .addMethod(MethodSpec.methodBuilder("toString")
                                        .addAnnotation(Override.class)
                                        .returns(String.class)
                                        .addModifiers(Modifier.PUBLIC)
                                        .addStatement("return value")
                                        .build())
                                .addMethod(MethodSpec.constructorBuilder()
                                        .addParameter(String.class, "value")
                                        .addStatement("this.$N = $N", "value", "value")
                                        .build());

                        List<String> constants = (List<String>) parameter.get("enum");

                        if ("DownloadBehavior".equals(enumName)) {
                            constants.add("allowAndName");
                        }

                        for (String next2 : constants) {
                            String cname = toCamelCase(next2);
                            if (cname.contains("-")) {
                                cname = cname.replace('-', '_');
                            }
                            if (Character.isDigit(cname.charAt(0))) {
                                cname = "_" + cname;
                            }
                            enumBuilder.addEnumConstant(
                                    cname,
                                    TypeSpec.anonymousClassBuilder("$S", next2)
                                            .addAnnotation(AnnotationSpec.builder(SerializedName.class)
                                                    .addMember("value", "\"" + next2 + "\"")
                                                    .build())
                                            .addAnnotation(AnnotationSpec.builder(JsonProperty.class)
                                                    .addMember("value", "\"" + next2 + "\"")
                                                    .build())
                                            .build());
                        }

                        TypeSpec typeSpec = enumBuilder.build();
                        writeType("constant", typeSpec, false, false);

                        typeName = ClassName.get("com.cdp4j.type.constant", enumName);
                    }

                    if (parameter.containsKey("$ref")) {
                        String ref = (String) parameter.get("$ref");
                        String refDomain = domain;
                        String refType = ref;
                        int start = ref.lastIndexOf(".");
                        if (start > 0) {
                            refDomain = ref.substring(0, start);
                            refType = ref.substring(start + 1, ref.length());
                        }
                        Map<String, Object> modelRefType = getRefType(refDomain, refType);
                        type = (String) modelRefType.get("type");
                        if ("string".equals(modelRefType.get("type")) && modelRefType.containsKey("enum")) {
                            typeName =
                                    ClassName.get("com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                        }
                    }

                    com.squareup.javapoet.ParameterSpec.Builder paramSpec =
                            ParameterSpec.builder(typeName.box(), (String) paramName);

                    if (exprParam) {
                        paramSpec.addAnnotation(Experimental.class);
                    }

                    if (optional) {
                        paramSpec.addAnnotation(Optional.class);
                    }
                    if (deprecated) {
                        paramSpec.addAnnotation(Deprecated.class);
                    }

                    methodBuilder.addParameter(paramSpec.build());
                    methodSignatureBuilder.addParameter(ParameterSpec.builder(typeName.box(), "_placeholder_")
                            .build());
                }
            }

            String methodDescription = (String) next.get("description");
            if (methodDescription != null) {
                methodDescription = methodDescription.replace("$x", "");
                methodDescription =
                        methodDescription.replaceAll("<code>", "<tt>").replaceAll("</code>", "</tt>");

                com.squareup.javapoet.CodeBlock.Builder codeBlockJavaDoc = CodeBlock.builder();
                codeBlockJavaDoc.add("$L", methodDescription.replace("`", "").trim());

                if (next.containsKey("parameters")) {
                    codeBlockJavaDoc.add("\r\n");
                    codeBlockJavaDoc.add("\r\n");
                    List<Map<String, Object>> parameters = (List<Map<String, Object>>) next.get("parameters");
                    for (Map<String, Object> parameter : parameters) {
                        String description = (String) parameter.get("description");
                        if (description != null) {
                            description = description.replace("$x", "x") + "\r\n";
                            final Object paramName = parameter.get("name");
                            if (skippedParamNames.contains(paramName)) {
                                continue;
                            }
                            codeBlockJavaDoc.add("@param $L $L", paramName, description.replace("`", ""));
                        }
                    }
                } else {
                    codeBlockJavaDoc.add("\r\n");
                }

                if (next.containsKey("returns")) {
                    List<Map<String, Object>> retList = (List<Map<String, Object>>) next.get("returns");
                    if (retList != null) {
                        if (retList.size() == 1) {
                            String retDescription = (String) retList.get(0).get("description");
                            if (retDescription != null) {
                                codeBlockJavaDoc.add("\r\n");
                                codeBlockJavaDoc.add(
                                        "@return $L",
                                        retDescription.replace("`", "").trim());
                            }
                        }
                    }
                }

                methodBuilder.addJavadoc(codeBlockJavaDoc.build());
            }

            boolean base64 = false;

            List<Map<String, Object>> returns = (List<Map<String, Object>>) next.get("returns");
            if (returns != null) {
                if (returns.size() == 1) {
                    Map<String, Object> parameter = returns.get(0);

                    String retDesc = (String) parameter.get("description");
                    if (retDesc != null && retDesc.contains("Base64-encoded")) {
                        base64 = true;
                    }

                    boolean isRef = parameter.containsKey("$ref");
                    String type = (String) parameter.get("type");

                    TypeName typeName = null;

                    boolean isMap = false;
                    boolean isArray = false;

                    if ("array".equals(type) && !isRef) {
                        Map<String, Object> items = (Map<String, Object>) parameter.get("items");
                        if (items.containsKey("$ref")) {
                            parameter = items;
                            isRef = true;
                            isArray = true;
                        }
                    }

                    if (isRef) {
                        String ref = (String) parameter.get("$ref");
                        String refDomain = domain;
                        String refType = ref;
                        int start = ref.lastIndexOf(".");
                        if (start > 0) {
                            refDomain = ref.substring(0, start);
                            refType = ref.substring(start + 1, ref.length());
                        }
                        Map<String, Object> modelRefType = getRefType(refDomain, refType);
                        if (modelRefType != null) {
                            type = (String) modelRefType.get("type");

                            if ("object".equals(type)) {

                                isMap = (refDomain + "." + refType).equals("Network.Headers") ? true : false;

                                if (isMap) {
                                    typeName = ParameterizedTypeName.get(Map.class, String.class, Object.class);
                                } else {
                                    typeName = ClassName.get(
                                            "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                                }
                            }

                            if ("array".equals(type) || isArray) {
                                Map<String, Object> items = (Map<String, Object>) modelRefType.get("items");
                                if (items != null) {
                                    String parameterizedType = (String) items.get("type");
                                    TypeName parameterizedJavaType = toJavaType(parameterizedType);
                                    if (parameterizedJavaType != null) {
                                        typeName = ParameterizedTypeName.get(
                                                ClassName.get(List.class), parameterizedJavaType.box());
                                        if (isRef) {
                                            Class<?> javaType2 = (Class<?>) toJavaType2(parameterizedType);
                                            typeName = ParameterizedTypeName.get(
                                                    ClassName.get(List.class),
                                                    ParameterizedTypeName.get(List.class, javaType2));
                                        }
                                        isArray = true;
                                    }
                                } else {
                                    if (modelRefType.containsKey("id")) {

                                        TypeName originalRefType = toJavaType(refType);
                                        if (originalRefType == null && modelRefType.containsKey("type")) {
                                            originalRefType = toJavaType((String) modelRefType.get("type"));
                                            if (originalRefType != null
                                                    && originalRefType
                                                            .toString()
                                                            .equals("int")) {
                                                originalRefType = originalRefType.box();
                                            }
                                        }

                                        if (originalRefType == null) {
                                            originalRefType = ClassName.get(
                                                    "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                                        }

                                        typeName =
                                                ParameterizedTypeName.get(ClassName.get(List.class), originalRefType);
                                    }
                                }
                            }
                        }
                    } else {
                        typeName = toJavaType(type);
                        if (typeName != null) {
                            if (typeName.isBoxedPrimitive()) {
                                typeName = typeName.box();
                            }
                        }
                    }

                    if (typeName == null) {
                        typeName = toJavaType(type);
                        if (typeName != null) {
                            if (typeName.isBoxedPrimitive()) {
                                typeName = typeName.box();
                            }
                        }
                    }

                    if ("this".equals(name)) {
                        name = "that";
                    }

                    Map<String, Object> items = (Map<String, Object>) parameter.get("items");
                    if (typeName == null && items != null) {
                        String parameterizedType = (String) items.get("type");
                        TypeName parameterizedJavaType = toJavaType(parameterizedType);
                        if (parameterizedJavaType != null) {
                            typeName =
                                    ParameterizedTypeName.get(ClassName.get(List.class), parameterizedJavaType.box());
                            isArray = true;
                        }
                    }

                    if (typeName == null) {
                        continue;
                    }

                    if (base64) {
                        typeName = TypeName.get(byte[].class);
                    }

                    String returnName = (String) returns.get(0).get("name");
                    if (returnName != null) {
                        String cmdLine = domain + ", " + name + ", " + returnName + "\r\n";
                        try {
                            if (!COMMANDS.contains(cmdLine)) {
                                COMMANDS.add(cmdLine);
                                Files.write(
                                        Paths.get("commands.txt"),
                                        cmdLine.getBytes(StandardCharsets.UTF_8),
                                        StandardOpenOption.APPEND,
                                        StandardOpenOption.CREATE);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    methodBuilder.returns(typeName);
                    methodSignatureBuilder.returns(typeName);
                } else if (returns.size() > 1) {

                    String returnClassName = toCamelCase(name) + "Result";
                    Builder returnClassBuilder =
                            TypeSpec.classBuilder(returnClassName).addModifiers(Modifier.PUBLIC);

                    Iterator<Map<String, Object>> iter = returns.iterator();
                    while (iter.hasNext()) {
                        Map<String, Object> parameter = iter.next();

                        String paramName = (String) parameter.get("name");

                        String description = (String) parameter.get("description");

                        boolean isBase64 = false;
                        if ("PrintToPDFResult".equals(returnClassName) && "data".equals(paramName)) {
                            isBase64 = true;
                        }

                        boolean isRef = parameter.containsKey("$ref");
                        String type = (String) parameter.get("type");

                        TypeName typeName = null;

                        boolean isMap = false;
                        boolean isArray = false;

                        if ("array".equals(type) && !isRef) {
                            Map<String, Object> items = (Map<String, Object>) parameter.get("items");
                            if (items.containsKey("$ref")) {
                                parameter = items;
                                isRef = true;
                                isArray = true;
                            }
                        }

                        if (isRef) {
                            String ref = (String) parameter.get("$ref");
                            String refDomain = domain;
                            String refType = ref;
                            int start = ref.lastIndexOf(".");
                            if (start > 0) {
                                refDomain = ref.substring(0, start);
                                refType = ref.substring(start + 1, ref.length());
                            }
                            Map<String, Object> modelRefType = getRefType(refDomain, refType);
                            if (modelRefType != null) {
                                type = (String) modelRefType.get("type");

                                if ("object".equals(type)) {

                                    isMap = (refDomain + "." + refType).equals("Network.Headers") ? true : false;

                                    if (isMap) {
                                        typeName = ParameterizedTypeName.get(Map.class, String.class, Object.class);
                                    } else {
                                        typeName = ClassName.get(
                                                "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                                    }
                                }

                                if ("array".equals(type) || isArray) {
                                    Map<String, Object> items = (Map<String, Object>) modelRefType.get("items");
                                    if (items != null) {
                                        String parameterizedType = (String) items.get("type");
                                        TypeName parameterizedJavaType = toJavaType(parameterizedType);
                                        if (parameterizedJavaType != null) {
                                            typeName = ParameterizedTypeName.get(
                                                    ClassName.get(List.class), parameterizedJavaType.box());
                                            isArray = true;
                                        }
                                    } else {
                                        if (modelRefType.containsKey("id")) {

                                            TypeName originalRefType = toJavaType(refType);
                                            if (originalRefType == null && modelRefType.containsKey("type")) {
                                                originalRefType = toJavaType((String) modelRefType.get("type"));
                                                if (originalRefType != null
                                                        && originalRefType
                                                                .toString()
                                                                .equals("int")) {
                                                    originalRefType = originalRefType.box();
                                                }
                                            }

                                            if (originalRefType == null) {
                                                originalRefType = ClassName.get(
                                                        "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH),
                                                        refType);
                                            }

                                            typeName = ParameterizedTypeName.get(
                                                    ClassName.get(List.class), originalRefType);
                                        }
                                    }
                                }
                            }
                        } else {
                            typeName = toJavaType(type);
                            if (typeName != null) {
                                if (typeName.isBoxedPrimitive()) {
                                    typeName = typeName.box();
                                }
                            }
                            if (isBase64) {
                                typeName = ArrayTypeName.get(byte[].class);
                            }
                        }

                        if (paramName.equals("strings") && "array".equals(type) && parameter.containsKey("items")) {
                            typeName = ParameterizedTypeName.get(List.class, String.class);
                        }

                        if (typeName == null) {
                            typeName = toJavaType(type);
                            if (typeName != null) {
                                if (typeName.isBoxedPrimitive()) {
                                    typeName = typeName.box();
                                }
                            }
                        }

                        if ("this".equals(name)) {
                            name = "that";
                        }

                        if (typeName == null) {
                            continue;
                        }

                        returnClassBuilder.addField(FieldSpec.builder(typeName, paramName)
                                .addModifiers(Modifier.PRIVATE)
                                .build());

                        com.squareup.javapoet.MethodSpec.Builder getterBuilder = MethodSpec.methodBuilder(
                                        "get" + toCamelCase(paramName))
                                .addModifiers(Modifier.PUBLIC)
                                .returns(typeName)
                                .addStatement("return " + paramName);

                        if (description != null && !description.trim().isEmpty()) {
                            getterBuilder.addJavadoc(
                                    "$L", description.replace("`", "").trim());
                        }

                        returnClassBuilder.addMethod(getterBuilder.build());

                        com.squareup.javapoet.MethodSpec.Builder setterBuilder = MethodSpec.methodBuilder(
                                        "set" + toCamelCase(paramName))
                                .addModifiers(Modifier.PUBLIC)
                                .addParameter(typeName, paramName)
                                .addStatement("this." + paramName + " = " + paramName);

                        if (description != null && !description.trim().isEmpty()) {
                            setterBuilder.addJavadoc(
                                    "$L", description.replace("`", "").trim());
                        }

                        returnClassBuilder.addMethod(setterBuilder.build());
                    }

                    writeType(domain, returnClassBuilder.build(), false, false);

                    ClassName returnType =
                            ClassName.get("com.cdp4j.type." + domain.toLowerCase(Locale.ENGLISH), returnClassName);

                    methodBuilder.addJavadoc("\r\n@return " + returnClassName + "\r\n");
                    methodBuilder.returns(returnType);
                    methodSignatureBuilder.returns(returnType);
                }
            }

            final String signature = methodSignatureBuilder.build().toString();
            if (signatures.contains(signature)) {
                // avoid to generate times the same method
                continue;
            } else {
                signatures.add(signature);
            }

            methodBuilder.addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT);
            builder.addMethod(methodBuilder.build());
        }

        TypeSpec typeSpec = builder.build();
        writeCommand(domain, typeSpec);
    }

    @SuppressWarnings("unchecked")
    protected static void generateBean(
            final String domain,
            final List<Map<String, Object>> properties,
            final String className,
            String cdesc,
            final boolean exp,
            final String id) {

        boolean addStaticImportEmptyList = false;
        boolean addStaticImportEmptyHashMap = false;

        Builder builder = TypeSpec.classBuilder(className).addModifiers(Modifier.PUBLIC);

        if (exp) {
            builder.addAnnotation(Experimental.class);
        }

        if (cdesc != null) {
            if (cdesc != null) {
                cdesc = cdesc.replace("$x", "x");
                builder.addJavadoc("$L", cdesc.replace("`", ""));
            }
        }

        for (Map<String, Object> next : properties) {
            boolean isRef = next.containsKey("$ref");
            String name = (String) next.get("name");
            String type = (String) next.get("type");
            String pdesc = (String) next.get("description");

            boolean deprecated = Boolean.TRUE.equals(next.get("deprecated"));

            TypeName typeName = null;

            boolean isMap = false;
            boolean isArray = false;

            if ("array".equals(type) && !isRef) {
                Map<String, Object> items = (Map<String, Object>) next.get("items");
                if (items.containsKey("$ref")) {
                    next = items;
                    isRef = true;
                    isArray = true;
                }
            }

            if (isRef) {
                String ref = (String) next.get("$ref");

                String refDomain = domain;
                String refType = ref;
                int start = ref.lastIndexOf(".");
                if (start > 0) {
                    refDomain = ref.substring(0, start);
                    refType = ref.substring(start + 1, ref.length());
                }

                Map<String, Object> modelRefType = getRefType(refDomain, refType);
                if (modelRefType != null) {

                    type = (String) modelRefType.get("type");

                    // List<List<?>>
                    boolean isArrayInArray =
                            ("ArrayOfStrings".equals(refType) || "Rectangle".equals(refType)) && "array".equals(type);

                    if ("object".equals(type)) {
                        isMap = (refDomain + "." + refType).equals("Network.Headers") ? true : false;
                        if (isMap) {
                            typeName = ParameterizedTypeName.get(Map.class, String.class, Object.class);
                        } else if ("MemoryDumpConfig".equals(modelRefType.get("id"))) {
                            typeName = ClassName.get(Object.class);
                        } else {
                            typeName =
                                    ClassName.get("com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                        }

                    } else if ("array".equals(type) || isArray) {
                        String modelRefTypeValue = (String) modelRefType.get("type");
                        Map<String, Object> items = (Map<String, Object>) modelRefType.get("items");
                        if (items != null) {
                            String parameterizedType = (String) items.get("type");
                            String $ref = (String) items.get("$ref");
                            if (parameterizedType == null && $ref != null) {
                                Map<String, Object> refTypeSub = getRefType(refDomain, $ref);
                                String typeSub = (String) refTypeSub.get("type");
                                if (typeSub != null) {
                                    parameterizedType = typeSub;
                                }
                            }
                            TypeName parameterizedJavaType = toJavaType(parameterizedType);
                            if (parameterizedJavaType != null && !isArrayInArray) {
                                typeName = ParameterizedTypeName.get(
                                        ClassName.get(List.class), parameterizedJavaType.box());
                                isArray = true;
                            } else if (parameterizedJavaType != null && isArrayInArray) {
                                ParameterizedTypeName arrayInArrayParameterized = ParameterizedTypeName.get(
                                        ClassName.get(List.class), parameterizedJavaType.box());
                                typeName =
                                        ParameterizedTypeName.get(ClassName.get(List.class), arrayInArrayParameterized);
                                isArray = true;
                            }
                        } else if ("string".equals(modelRefTypeValue) || "integer".equals(modelRefTypeValue)) {
                            TypeName parameterizedJavaType = null;
                            if (modelRefType.containsKey("enum")) {
                                parameterizedJavaType = ClassName.get(
                                        "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                            } else {
                                parameterizedJavaType = toJavaType(modelRefTypeValue);
                            }
                            if (parameterizedJavaType != null) {
                                typeName = ParameterizedTypeName.get(
                                        ClassName.get(List.class), parameterizedJavaType.box());
                            }
                        } else {
                            if (modelRefType.containsKey("id")) {
                                typeName = ParameterizedTypeName.get(
                                        ClassName.get(List.class),
                                        ClassName.get(
                                                "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType));
                            }
                        }
                    }
                }

                if ("string".equals(type) && modelRefType.containsKey("enum") && typeName == null) {
                    typeName = ClassName.get("com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                } else {
                    if (typeName == null) {
                        typeName = toJavaType(type);
                    }
                }
            } else {
                typeName = toJavaType(type);
            }

            if ("array".equals(type)) {
                Object itemsO = next.get("items");
                if (itemsO != null) {
                    Map<String, Object> items = (Map<String, Object>) itemsO;
                    String originalType = (String) items.get("type");
                    TypeName originalJavaType = toJavaType(originalType);
                    if (originalJavaType != null) {
                        typeName = ParameterizedTypeName.get(ClassName.get(List.class), originalJavaType.box());
                        isArray = true;
                    }
                }
            }

            if (typeName == null && "object".equals(type)) {
                typeName = TypeName.OBJECT;
            }

            if ("ExecutionContextDescription".equals(className) && "auxData".equals(name)) {
                typeName = ParameterizedTypeName.get(
                        ClassName.get(Map.class), ClassName.get(String.class), ClassName.get(Object.class));
            }

            if (type.equals("string")) {
                for (Map<String, Object> m : properties) {
                    List<String> enm = (List<String>) m.get("enum");
                    if (enm != null && !enm.isEmpty()) {

                        String enumName = "";
                        String desc = (String) m.get("description");

                        String nameeee = (String) m.get("name");

                        if (!name.equals(nameeee)) {
                            continue;
                        }

                        if (enm.contains("portraitPrimary")) {
                            enumName = "PortraitType";
                        }

                        if ("Scope type.".equals(desc)) {
                            enumName = "ScopeType";
                        }

                        if ("Message source.".equals(desc)) {
                            enumName = "MessageSource";
                        }

                        if ("Message severity.".equals(desc)) {
                            enumName = "MessageSeverity";
                        }

                        if ("Object type.".equals(desc)) {
                            enumName = "ObjectType";
                        }

                        if (desc != null && desc.startsWith("Object subtype hint.")) {
                            enumName = "ObjectSubtypeHint";
                        }

                        if ("Key path type.".equals(desc)) {
                            enumName = "KeyPathType";
                        }

                        if ("Key type.".equals(desc)) {
                            enumName = "KeyType";
                        }

                        if (enm.contains("debuggerStatement")) {
                            enumName = "BreakLocationType";
                        }

                        if ("Log entry severity.".equals(desc)) {
                            enumName = "LogEntrySeverity";
                        }

                        if ("Log entry source.".equals(desc)) {
                            enumName = "LogEntrySource";
                        }

                        if ("Animation type of `Animation`.".equals(desc)) {
                            enumName = "AnimationType";
                        }

                        if (enm.contains("RepaintsOnScroll")) {
                            enumName = "RepaintReason";
                        }

                        if (enm.contains("recordUntilFull")) {
                            enumName = "TraceRecordMode";
                        }

                        if (nameeee.equals("referrerPolicy")) {
                            enumName = "ReferrerPolicy";
                        }

                        if (nameeee.equals("mixedContentType")) {
                            enumName = "MixedContentType";
                        }

                        if ("Type of this initiator.".equals(desc)) {
                            enumName = "InitiatorType";
                        }

                        if (enm.contains("touchPressed")) {
                            enumName = "TouchPointState";
                        }

                        if (desc != null && desc.startsWith("Source of the media query: \"mediaRule\"")) {
                            enumName = "CSSMediaSource";
                        }

                        if (desc != null && desc.startsWith("Object type. Accessor means that the")) {
                            enumName = "PropertyPreviewType";
                        }

                        if ("Violation type.".equals(desc)) {
                            enumName = "ViolationType";
                        }

                        if (desc != null && desc.equals("Orientation of a display feature in relation to screen")) {
                            enumName = "DisplayOrientation";
                        }

                        if (desc != null
                                && desc.equals("Image compression format (defaults to png).")
                                && "format".equals(nameeee)) {
                            enumName = "ImageFormat";
                        }

                        if ("refreshPolicy".endsWith(name)
                                && desc != null
                                && desc.startsWith("Only set for \"token-redemption\"")) {
                            enumName = "TrustTokenRefreshPolicy";
                        }

                        if ("source".equals(name) && "Source of the authentication challenge.".equals(desc)) {
                            enumName = "AuthChallengeSource";
                        }

                        if ("response".endsWith(name)
                                && desc != null
                                && desc.startsWith("The decision on what to do in response to the authorization")) {
                            enumName = "AuthResponse";
                        }

                        if ("pattern".equals(name)
                                && desc != null
                                && desc.startsWith("The line pattern (default: solid)")) {
                            enumName = "LineStylePattern";
                        }

                        if ("level".equals(name)
                                && desc != null
                                && desc.startsWith("Keep in sync with MediaLogMessageLevel")) {
                            enumName = "MediaLogMessageLevel";
                        }

                        if ("type".equals(name)
                                && "Corresponds to kMediaError".equals(cdesc)
                                && enm.contains("pipeline_error")
                                && enm.contains("media_error")) {
                            enumName = "PlayerError";
                        }

                        if ("type".equals(name) && "Type of the debug symbols.".equals(desc)) {
                            enumName = "DebugSymbolType";
                        }

                        if ("category".equals(name) && "Log".equals(domain)) {
                            enumName = "LogCategory";
                        }

                        if ((enumName == null || enumName.trim().isEmpty())
                                && "string".equals(type)
                                && id != null
                                && id.length() > 0
                                && Character.isUpperCase(id.charAt(0))) {
                            enumName = id + "Type";
                        }

                        // Debug output in case of further
                        if (enumName == null || enumName.trim().isEmpty()) {
                            System.out.println(name + ", desc: " + desc + ", id: " + id + ", type: " + type);
                        }

                        Builder enumBuilder = TypeSpec.enumBuilder(enumName)
                                .addModifiers(Modifier.PUBLIC)
                                .addField(String.class, "value", Modifier.PUBLIC, Modifier.FINAL)
                                .addMethod(MethodSpec.methodBuilder("toString")
                                        .addAnnotation(Override.class)
                                        .returns(String.class)
                                        .addModifiers(Modifier.PUBLIC)
                                        .addStatement("return value")
                                        .build())
                                .addMethod(MethodSpec.constructorBuilder()
                                        .addParameter(String.class, "value")
                                        .addStatement("this.$N = $N", "value", "value")
                                        .build());

                        if (desc != null) {
                            enumBuilder.addJavadoc("$L", desc.replace("`", "").trim());
                        }

                        for (String next2 : enm) {
                            String cname = toCamelCase(next2);
                            if (cname.contains("-")) {
                                cname = cname.replace('-', '_');
                            }
                            if (Character.isDigit(cname.charAt(0))) {
                                cname = "_" + cname;
                            }
                            enumBuilder.addEnumConstant(
                                    cname,
                                    TypeSpec.anonymousClassBuilder("$S", next2)
                                            .addAnnotation(AnnotationSpec.builder(SerializedName.class)
                                                    .addMember("value", "\"" + next2 + "\"")
                                                    .build())
                                            .addAnnotation(AnnotationSpec.builder(JsonProperty.class)
                                                    .addMember("value", "\"" + next2 + "\"")
                                                    .build())
                                            .build());
                        }

                        TypeSpec typeSpec = enumBuilder.build();
                        writeType("constant", typeSpec, false, false);

                        typeName = ClassName.get("com.cdp4j.type.constant", enumName);
                    }
                }
            }

            if (isArray && "object".equals(type) && typeName != null) {
                typeName = ParameterizedTypeName.get(ClassName.get(List.class), typeName);
            }

            if (typeName == null) {
                continue;
            }

            if ("this".equals(name)) {
                name = "that";
            }

            com.squareup.javapoet.FieldSpec.Builder fieldBuilder = FieldSpec.builder(typeName, name, Modifier.PRIVATE);

            if (isMap) {
                fieldBuilder.initializer("$L", "emptyMap()");
                addStaticImportEmptyHashMap = true;
            } else if (isArray) {
                fieldBuilder.initializer("$L", "emptyList()");
                addStaticImportEmptyList = true;
            }

            if ("that".equals(name)) {
                AnnotationSpec that = AnnotationSpec.builder(SerializedName.class)
                        .addMember("value", "\"this\"")
                        .build();
                fieldBuilder.addAnnotation(that);
                AnnotationSpec thatJackson = AnnotationSpec.builder(JsonProperty.class)
                        .addMember("value", "\"this\"")
                        .build();
                fieldBuilder.addAnnotation(thatJackson);
            }

            FieldSpec field = fieldBuilder.build();

            builder.addField(field);

            String getterPrefix = "boolean".equals(type) ? "is" : "get";

            com.squareup.javapoet.MethodSpec.Builder getter = MethodSpec.methodBuilder(getterPrefix + toCamelCase(name))
                    .addModifiers(Modifier.PUBLIC)
                    .addStatement("return " + name)
                    .returns(typeName);

            if (deprecated) {
                getter.addAnnotation(Deprecated.class);
            }

            if (pdesc != null && !pdesc.trim().isEmpty()) {
                getter.addJavadoc(pdesc.replace("`", ""));
                getter.addJavadoc("\r\n");
            }

            builder.addMethod(getter.build());

            com.squareup.javapoet.MethodSpec.Builder setter = MethodSpec.methodBuilder("set" + toCamelCase(name))
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(typeName, name)
                    .addStatement("this." + name + " = " + name);

            if (deprecated) {
                setter.addAnnotation(Deprecated.class);
            }

            if (pdesc != null && !pdesc.trim().isEmpty()) {
                setter.addJavadoc(pdesc.replace("`", ""));
                setter.addJavadoc("\r\n");
            }

            builder.addMethod(setter.build());
        }

        if ("CookiePartitionKey".equals(className)) { // changed type in r1311068 (Chrome 127)
            builder.addMethod(MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .build()); // Default constructor for newer versions
            builder.addMethod(MethodSpec.constructorBuilder()
                    .addModifiers(Modifier.PUBLIC)
                    .addParameter(TypeName.get(String.class), "topLevelSite")
                    .addStatement("this.topLevelSite = topLevelSite")
                    .build());
        }

        TypeSpec typeSpec = builder.build();
        writeType(domain, typeSpec, addStaticImportEmptyList, addStaticImportEmptyHashMap);
    }

    @SuppressWarnings("unchecked")
    protected static void generateEvent(
            final String domain,
            final List<Map<String, Object>> properties,
            final String className,
            String cdesc,
            boolean exp) {

        Builder builder = TypeSpec.classBuilder(toCamelCase(className)).addModifiers(Modifier.PUBLIC);

        if (exp) {
            builder.addAnnotation(Experimental.class);
        }

        if (cdesc != null) {
            if (cdesc != null) {
                cdesc = cdesc.replace("$x", "x");
                builder.addJavadoc("$L", cdesc.replace("`", ""));
            }
        }

        if (properties != null) {
            for (Map<String, Object> next : properties) {
                boolean isRef = next.containsKey("$ref");
                String name = (String) next.get("name");
                String type = (String) next.get("type");
                String pdesc = (String) next.get("description");

                TypeName typeName = null;

                boolean isMap = false;
                boolean isArray = false;

                if ("array".equals(type) && !isRef) {
                    Map<String, Object> items = (Map<String, Object>) next.get("items");
                    if (items.containsKey("$ref")) {
                        next = items;
                        isRef = true;
                        isArray = true;
                    }
                }

                if (isRef) {
                    String ref = (String) next.get("$ref");

                    String refDomain = domain;
                    String refType = ref;
                    int start = ref.lastIndexOf(".");
                    if (start > 0) {
                        refDomain = ref.substring(0, start);
                        refType = ref.substring(start + 1, ref.length());
                    }
                    Map<String, Object> modelRefType = getRefType(refDomain, refType);
                    if (modelRefType != null) {
                        type = (String) modelRefType.get("type");

                        if ("object".equals(type)) {

                            isMap = (refDomain + "." + refType).equals("Network.Headers") ? true : false;

                            if (isMap) {
                                typeName = ParameterizedTypeName.get(Map.class, String.class, Object.class);
                            } else {
                                typeName = ClassName.get(
                                        "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                            }
                        }

                        if ("array".equals(type) || isArray) {
                            String type2 = (String) modelRefType.get("type");
                            if ("integer".equals(type2)
                                    || "string".equals(type2)
                                    || "number".equals(type2)
                                    || "boolean".equals(type2)) {
                                TypeName parameterizedJavaType = toJavaType(type2);
                                if (parameterizedJavaType != null) {
                                    typeName = ParameterizedTypeName.get(
                                            ClassName.get(List.class), parameterizedJavaType.box());
                                    isArray = true;
                                }
                            } else {
                                if (modelRefType.containsKey("id")) {
                                    typeName = ParameterizedTypeName.get(
                                            ClassName.get(List.class),
                                            ClassName.get(
                                                    "com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH),
                                                    refType));
                                }
                            }
                        }
                    }

                    if ("string".equals(type) && modelRefType.containsKey("enum")) {
                        typeName = ClassName.get("com.cdp4j.type." + refDomain.toLowerCase(Locale.ENGLISH), refType);
                    } else {
                        if (typeName == null) {
                            typeName = toJavaType(type);
                        }
                    }
                } else {
                    typeName = toJavaType(type);
                }

                if ("array".equals(type)) {
                    Object itemsO = next.get("items");
                    if (itemsO != null) {
                        Map<String, Object> items = (Map<String, Object>) itemsO;
                        String originalType = (String) items.get("type");
                        TypeName originalJavaType = toJavaType(originalType);
                        if (originalJavaType != null) {
                            typeName = ParameterizedTypeName.get(ClassName.get(List.class), originalJavaType.box());
                        }
                    }
                }

                if ("value".equals(name) && "dataCollected".equals(className)) {
                    typeName = ParameterizedTypeName.get(
                            ClassName.get(List.class),
                            ParameterizedTypeName.get(Map.class, String.class, Object.class));
                }

                if (typeName == null && "object".equals(type)) {
                    typeName = TypeName.OBJECT;
                }

                if (type.equals("string")) {
                    for (Map<String, Object> m : properties) {
                        List<String> enm = (List<String>) m.get("enum");
                        if (enm != null && !enm.isEmpty()) {

                            String enumName = "";
                            String desc = (String) m.get("description");

                            String nameeee = (String) m.get("name");

                            if (!name.equals(nameeee)) {
                                continue;
                            }

                            if (enm.contains("trace") && desc.equals("Type of the call.")) {
                                enumName = "ConsoleApiCallType";
                            }

                            if ("reason".equals(name) && "Pause reason.".equals(desc)) {
                                enumName = "PauseReason";
                            }

                            if ("status".equals(name)
                                    && desc != null
                                    && desc.startsWith("Detailed success or error status of the operation.")) {
                                enumName = "TrustTokenStatus";
                            }

                            if ("mode".equals(name) && "Input mode.".equals(desc)) {
                                enumName = "FileChooserInputMode";
                            }

                            if ("reason".equals(name)
                                    && enm.contains("remove")
                                    && enm.contains("swap")
                                    && cdesc != null
                                    && cdesc.startsWith("Fired when frame has been detached")) {
                                enumName = "FrameDetachedReason";
                            }

                            if (name.equals("state") && "Download status.".equals(desc)) {
                                enumName = "DownloadState";
                            }

                            if (name.equals("navigationType")) {
                                if (enm.contains("fragment")) {
                                    enumName = "NavigationWithinDocumentType"; // not to confuse it with Page.NavigationType
                                } else if (enm.contains("reloadBypassingCache")) {
                                    enumName = "FrameStartedNavigatingNavigationType"; // not to confuse it with Page.NavigationType
                                }
                            }

                            if (enumName == null || enumName.trim().isEmpty()) {
                                System.out.println("No enum name - another 'if' missing here?");
                                System.out.println("name: " + name);
                                System.out.println("desc: " + desc);
                                System.out.println("enm: " + enm);
                            }

                            Builder enumBuilder = TypeSpec.enumBuilder(enumName)
                                    .addModifiers(Modifier.PUBLIC)
                                    .addField(String.class, "value", Modifier.PUBLIC, Modifier.FINAL)
                                    .addMethod(MethodSpec.methodBuilder("toString")
                                            .addAnnotation(Override.class)
                                            .returns(String.class)
                                            .addModifiers(Modifier.PUBLIC)
                                            .addStatement("return value")
                                            .build())
                                    .addMethod(MethodSpec.constructorBuilder()
                                            .addParameter(String.class, "value")
                                            .addStatement("this.$N = $N", "value", "value")
                                            .build());

                            if (desc != null && desc.trim().isEmpty()) {
                                enumBuilder.addJavadoc(desc);
                            }

                            for (String next2 : enm) {
                                String cname = toCamelCase(next2);
                                if (cname.contains("-")) {
                                    cname = cname.replace('-', '_');
                                }
                                if (Character.isDigit(cname.charAt(0))) {
                                    cname = "_" + cname;
                                }
                                enumBuilder.addEnumConstant(
                                        cname,
                                        TypeSpec.anonymousClassBuilder("$S", next2)
                                                .addAnnotation(AnnotationSpec.builder(SerializedName.class)
                                                        .addMember("value", "\"" + next2 + "\"")
                                                        .build())
                                                .addAnnotation(AnnotationSpec.builder(JsonProperty.class)
                                                        .addMember("value", "\"" + next2 + "\"")
                                                        .build())
                                                .build());
                            }

                            TypeSpec typeSpec = enumBuilder.build();
                            writeType("constant", typeSpec, false, false);

                            typeName = ClassName.get("com.cdp4j.type.constant", enumName);
                        }
                    }
                }

                if (typeName == null) {
                    continue;
                }

                if ("this".equals(name)) {
                    name = "that";
                }

                com.squareup.javapoet.FieldSpec.Builder fieldBuilder =
                        FieldSpec.builder(typeName, name, Modifier.PRIVATE);

                if ("that".equals(name)) {
                    AnnotationSpec that = AnnotationSpec.builder(SerializedName.class)
                            .addMember("value", "\"this\"")
                            .build();
                    fieldBuilder.addAnnotation(that);
                }

                FieldSpec field = fieldBuilder.build();

                builder.addField(field);

                String getterPrefix = "boolean".equals(type) ? "is" : "get";

                com.squareup.javapoet.MethodSpec.Builder getter = MethodSpec.methodBuilder(
                                getterPrefix + toCamelCase(name))
                        .addModifiers(Modifier.PUBLIC)
                        .addStatement("return " + name)
                        .returns(typeName);

                if (pdesc != null && !pdesc.trim().isEmpty()) {
                    getter.addJavadoc("$L", pdesc.replace("`", ""));
                }

                builder.addMethod(getter.build());

                com.squareup.javapoet.MethodSpec.Builder setter = MethodSpec.methodBuilder("set" + toCamelCase(name))
                        .addModifiers(Modifier.PUBLIC)
                        .addParameter(typeName, name)
                        .addStatement("this." + name + " = " + name);

                if (pdesc != null && !pdesc.trim().isEmpty()) {
                    setter.addJavadoc("$L", pdesc.replace("`", ""));
                }

                builder.addMethod(setter.build());
            }
        }

        // AnnotationSpec domainAnnotation = AnnotationSpec.builder(Domain.class).addMember("value", "\"" + domain +
        // "\"").build();
        // builder.addAnnotation(domainAnnotation);

        // AnnotationSpec eventTypeAnnotation = AnnotationSpec.builder(EventName.class).addMember("value", "\"" +
        // className + "\"").build();
        // builder.addAnnotation(eventTypeAnnotation);

        TypeSpec typeSpec = builder.build();
        writeEvent(domain, typeSpec);
    }

    protected static void writeCommand(final String domain, TypeSpec typeSpec) {
        JavaFile javaFile = JavaFile.builder(COMMAND_PACKAGE, typeSpec)
                .addFileComment(LICENSE_TEXT_TEMPLATE)
                .skipJavaLangImports(true)
                .indent(Configuration.INDENT)
                .build();
        Path rootPath = Paths.get("src/main/java");
        try {
            javaFile.writeTo(rootPath);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        addLicenseHeader(javaFile, rootPath);
    }

    protected static void writeType(
            final String domain,
            TypeSpec typeSpec,
            boolean addStaticImportEmptyList,
            boolean addStaticImportEmptyHashMap) {
        com.squareup.javapoet.JavaFile.Builder javaFileBuilder = JavaFile.builder(
                        "com.cdp4j.type." + domain.toLowerCase(Locale.ENGLISH), typeSpec)
                .addFileComment(LICENSE_TEXT_TEMPLATE)
                .skipJavaLangImports(true)
                .indent(Configuration.INDENT);
        if (addStaticImportEmptyList) {
            javaFileBuilder.addStaticImport(Collections.class, "emptyList");
        }
        if (addStaticImportEmptyHashMap) {
            javaFileBuilder.addStaticImport(Collections.class, "emptyMap");
        }
        JavaFile javaFile = javaFileBuilder.build();
        Path file = Paths.get("src/main/java");
        try {
            javaFile.writeTo(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        addLicenseHeader(javaFile, file);
    }

    protected static void addLicenseHeader(JavaFile javaFile, Path rootPath) {
        Path javaFilePath = rootPath.toAbsolutePath()
                .resolve(javaFile.packageName.replace('.', File.separatorChar) + File.separator + javaFile.typeSpec.name
                        + ".java");
        List<String> lines;
        try {
            lines = Files.readAllLines(javaFilePath, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        lines = lines.subList(1, lines.size());
        lines.add(0, "// SPDX-License-Identifier: MIT");
        try {
            Files.write(javaFilePath, lines, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected static void writeEvent(final String domain, TypeSpec typeSpec) {
        JavaFile javaFile = JavaFile.builder("com.cdp4j.event." + domain.toLowerCase(Locale.ENGLISH), typeSpec)
                .skipJavaLangImports(true)
                .addFileComment(LICENSE_TEXT_TEMPLATE)
                .indent(Configuration.INDENT)
                .build();

        Path path = Paths.get("src/main/java");
        try {
            javaFile.writeTo(path);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        addLicenseHeader(javaFile, path);
    }

    @SuppressWarnings("unchecked")
    protected static Map<String, Object> getRefType(final String refDomain, final String refType) {
        for (Map<String, Object> next : domains) {
            if (next.containsKey("domain")) {
                String domain = (String) next.get("domain");
                if (domain.equals(refDomain)) {
                    if (next.containsKey("types")) {
                        if (isList(next, "types")) {
                            List<Map<String, Object>> types = (List<Map<String, Object>>) next.get("types");
                            for (Map<String, Object> type : types) {
                                if (refType.equals(type.get("id"))) {
                                    return type;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    protected static String toCamelCase(final String name) {
        final StringBuilder builder = new StringBuilder();
        boolean capitalizeNextChar = false;
        boolean first = true;
        for (int i = 0; i < name.length(); i++) {
            final char c = name.charAt(i);
            if (!isLetterOrDigit(c)) {
                if (!first) {
                    capitalizeNextChar = true;
                }
            } else {
                if (capitalizeNextChar || first || isUpperCase(c)) {
                    builder.append(toUpperCase(c));
                } else {
                    builder.append(toLowerCase(c));
                }
                capitalizeNextChar = false;
                first = false;
            }
        }
        return builder.toString();
    }

    protected static TypeName toJavaType(final String type) {
        if (type == null || type.trim().isEmpty()) {
            return null;
        }
        TypeName typeName = null;
        switch (type) {
            case "string":
                typeName = TypeName.get(String.class);
                break;
            case "integer":
                typeName = TypeName.INT.box();
                break;
            case "number":
                typeName = TypeName.DOUBLE.box();
                break;
            case "boolean":
                typeName = TypeName.BOOLEAN.box();
                break;
            case "any":
                typeName = TypeName.OBJECT.box();
                break;
            default: // Return value as a full qualified java type
                int idx = type.lastIndexOf('.');
                if (idx > 0) {
                    typeName = ClassName.get(type.substring(0, idx), type.substring(idx + 1));
                }
        }
        return typeName;
    }

    protected static Object toJavaType2(final String type) {
        if (type == null || type.trim().isEmpty()) {
            return null;
        }
        Object typeName = null;
        switch (type) {
            case "string":
                typeName = String.class;
                break;
            case "integer":
                typeName = Integer.class;
                break;
            case "number":
                typeName = Double.class;
                break;
            case "boolean":
                typeName = Boolean.class;
                break;
            case "any":
                typeName = Object.class;
                break;
        }
        return typeName;
    }

    protected static boolean toBoolean(Map<String, Object> map, String key) {
        return map.containsKey(key) ? ((Boolean) map.get(key)).booleanValue() : false;
    }

    protected static boolean isList(Map<String, Object> map, String key) {
        return map.containsKey(key)
                && map.get(key) != null
                && List.class.isAssignableFrom(map.get(key).getClass());
    }
}
