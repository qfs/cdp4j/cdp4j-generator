package com.cdp4j.generator;

import java.util.LinkedList;
import java.util.List;

public class Configuration {
    public static final String INDENT = "    ";

    public static final List<CustomParser> CUSTOM_PARSERS = new LinkedList<>();

    static {
        CUSTOM_PARSERS.add(
                new CustomParser("CustomCommand_getSnapshot", "DOMSnapshot_getSnapshot", "GetSnapshotParser"));
    }
}
