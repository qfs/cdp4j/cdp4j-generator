package com.cdp4j.generator;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.jboss.forge.roaster.Roaster;
import org.jboss.forge.roaster.model.JavaType;
import org.jboss.forge.roaster.model.source.FieldSource;
import org.jboss.forge.roaster.model.source.JavaClassSource;
import org.jboss.forge.roaster.model.source.MethodSource;

public class ToStringGenerator {

    public static void main(String[] args) throws IOException {
        List<Path> paths = new LinkedList<>();

        addSrcPaths("src/main/java/com/cdp4j/type", paths);
        addSrcPaths("src/main/java/com/cdp4j/event", paths);

        final Iterator<Path> iter = paths.iterator();
        while (iter.hasNext()) {
            Path srcPath = iter.next();
            JavaType source = Roaster.parse(JavaType.class, srcPath.toFile());
            if (source.isEnum() || source.isInterface()) {
                continue;
            }
            JavaClassSource classSource = (JavaClassSource) source;

            StringBuilder builder = new StringBuilder();
            if (classSource.getFields().isEmpty()) {
                continue;
            }
            MethodSource<JavaClassSource> existingToStringMethod = classSource.getMethod("toString");
            if (existingToStringMethod != null) {
                continue;
            }
            int idx = 0;
            for (FieldSource<?> fieldSource : classSource.getFields()) {
                String field = fieldSource.getName();
                if (idx == 0) {
                    builder.append("\"").append(classSource.getName()).append(" [");
                }
                if (idx >= 1) {
                    builder.append(" + ").append("\", ");
                }
                builder.append(field).append("=").append("\"").append(" + ").append(field);
                idx += 1;
            }
            builder.append(" + ").append("\"").append("]").append("\"").append(";");
            String toStringMethod = String.format("public String toString() { return %s }", builder.toString());
            classSource.addMethod(toStringMethod);
            Files.write(
                    srcPath,
                    classSource
                            .toString()
                            .replaceAll("\t", Configuration.INDENT)
                            .getBytes(StandardCharsets.UTF_8));
        }
    }

    private static void addSrcPaths(final String src, final List<Path> paths) throws IOException {
        Files.find(Paths.get(src), 999, (path, attributes) -> path.getFileName()
                        .toString()
                        .endsWith(".java"))
                .forEach(paths::add);
    }
}
