This application generates the necessary code by parsing the Chromium CDP protocol for the cdp4j project.

To use the application, you first need to compile it with Maven.

- For this do a "mvn install" in your local cdp4j project root and adapt version number in this project's pom.xml
- Then update the files in cdp4j/src/test/resources/protocol from https://github.com/ChromeDevTools/devtools-protocol/tree/master/json
- Then, you need to run the com.cdp4j.generator.Main class under the cdp4j project folder.

This application is only compatible with Java 11+.

Important Note: You must run the application under the cdp4j folder; otherwise, the generator will not work properly.

License:
cdp4j was a proprietary software, but was relicensed as Open Source in 2023 (see LICENSE file).

Since it is not available at the original source (https://github.com/cdp4j/generator) anymore, this fork has been created.